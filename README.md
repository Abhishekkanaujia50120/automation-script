# Automate SSL certificate and Domain validater
This tool helps to automate the entire process of Domain & SSL certificate expiry date check.

# Features
1. Easy to operate from one command line.
2. Generate a Date wise directory.
3. Inbuilt color-coded feature to make the output more appealing.
4. Generate a text-based output for both checks.
5. Built-in feature to capture the screenshot of the domain for SSL cert expiry.

# How to Execute
1. Download the repository 
2. Use Command: `chmod +x *`
3. To Run use Command: `bash script.sh`

# Basic Tool Requirements
1. Google-chrome
2. openssl
3. Whois

