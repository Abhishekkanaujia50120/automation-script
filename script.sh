#!/bin/bash
#For Dated-Result
date=$(date +'%e-%B-%T')
current_dir=$(pwd)
BLINK='\e[5m'
BOLD='\e[1m'
GREEN='\e[92m'
YELLOW='\e[93m'
CYAN='\e[96m'
RESET='\e[0m'
RED='\e[31m'
UNDERLINE='\e[4m'
PURPLE='\e[95m'
BLUE="\e[94m"

while true;do
	echo -e "\n"
	echo -e "${GREEN}Welcome to the SSL cert and Domain validate Automation Tool${RESET}"
	echo -e "Press ${CYAN}1${RESET} to enter the domain for SSL Certificate checker"
	echo -e "Press ${CYAN}2${RESET} to enter the domain for Domain valadity checker"
	echo -e "Press ${CYAN}3${RESET} to Run the SSL Certificate checker tool"
	echo -e "Press ${CYAN}4${RESET} to install the basic tool requirements"
	echo -e "Press ${CYAN}5${RESET} to Run the Domain renewal Check "
	echo -e "Press ${CYAN}0 | [Ctr+ C]${RESET} to Exit \n"
	
	read -p "Enter your Choise Please: " test
	echo -e "\n"


	check_command () {
	    	command -v "$1" >/dev/null
	}

	Create_dir(){
		echo $current_dir
		echo "Creating Folder on Dir"  
		mkdir -p $current_dir/${date}_SSl-Check
		cd ${date}_SSl-Check
		
        }

	basic_tool(){
			#For headless Chrome browser finding Google_chrome
			browser=$(which google-chrome)
			if [ ! -d "/usr/bin/google-chrome" ]; then
			    echo -e "${BOLD}${GREEN}Google Chrome is Already Installed... Good to Work!${RESET}"
			    else
			    echo "Installing Google Chrome!"
			    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
			    sudo dpkg -i google-chrome-stable_current_amd64.deb
				fi
			
			if ! check_command openssl; then
		    	echo -e "${BOLD}${GREEN}[+] Installing openssl...${RESET}"
		   		sudo apt install openssl
			else
	   	 		echo -e "${BOLD}${GREEN}[+]openssl is already installed ${RESET}"
			fi

			if ! check_command whois; then
		    	echo -e "${BOLD}${GREEN}[+] Installing Whois...${RESET}"
		   		sudo apt install whois
			else
	   	 		echo -e "${BOLD}${GREEN}[+]Whois is already installed ${RESET}"
			fi

	}


	read_input_for_ssl_cert(){
		echo "Please enter the all domains"
		nano $current_dir/ssl-cert-domain
	}

	read_input_for_domain(){
		echo "Please enter the all domains"
		nano $current_dir/domain_renew_list
	}

	ssl_expiry-date_left(){
		now_date=$( date +%s )
	    expiry_date=$( echo | openssl s_client -showcerts -connect $1:443 2>/dev/null | openssl x509 -inform pem -noout -enddate | cut -d "=" -f 2 )
	    expiry_date=$( date -d "$expiry_date" +%s )
	    expiry_days="$(( ($expiry_date - $now_date) / (3600 * 24) ))"
	    valid_from=$(echo | openssl s_client -connect $1:443 2>/dev/null | openssl x509 -noout -startdate)
	    exp_from=$(echo | openssl s_client -connect $1:443 2>/dev/null | openssl x509 -noout -enddate )
	    
	    echo  "SSl Cert Register date: $(echo $valid_from | cut -d '=' -f2)" | tee -a ${date}_ssl_report.txt
	    echo  "SSl Cert Expire date: $(echo $exp_from | cut -d '=' -f2)" | tee -a ${date}_ssl_report.txt
	    echo  "The certificate will expire in: $expiry_days days" | tee -a ${date}_ssl_report.txt
	}

	screenshoot(){
		/usr/bin/google-chrome --silent --headless --disable-gpu --timeout=30000 --hide-scrollbars --window-size=1920,1920 --screenshot="$1_ssl-cert.png" "https://www.sslshopper.com/ssl-checker.html#hostname=$1" > .temp
		rm .temp
	}

	processing(){
		for i in $(cat $current_dir/ssl-cert-domain); do
			echo -e "\n${BOLD}${RED}Checking SSL Certificate for $i Domain:${RESET}" 
			echo "Checking SSL Certificate for $i Domain:" >> ${date}_ssl_report.txt
			ssl_expiry-date_left $i
			echo -e "${BOLD}${RED}Capturing Screenshot for SSL Certificate:${RESET}"
			screenshoot $i
			echo -e "\n" | tee -a ${date}_ssl_report.txt
			echo -e "${BOLD}${GREEN}Sleep 5 seconds for rate limit prevent${RESET}"
			sleep 5
		done
	}


	Domain_renew(){
		for i in $(cat $current_dir/domain_renew_list); do
			echo "For Doamin: $i" | tee -a ${date}_Domain-renewal_report.txt
			whois $i | egrep -i 'Expiration|Expires on | Expiry' | head -1 | tee -a ${date}_Domain-renewal_report.txt 
			echo -e "\n" | tee -a ${date}_Domain-renewal_report.txt
		done
	}



	#Switch Statements for Controling Loop
		case $test in

		  1 ) read_input_for_ssl_cert	;;
	      
	      2 ) read_input_for_domain ;;
	      
	      3 ) Create_dir ; processing	;;
	      
	      4 ) basic_tool	;;

		  5 ) Create_dir; Domain_renew	;;
			
		  0 ) echo -e "${RED}Thanks for using..${RESET}"; break	;;

		  *) 	i=$((i+1))
				if [ $i == 5 ] ;then 
					echo -e "${RED}You have exceed your limit..!${RESET}"
					exit 
				else 
					echo  "Please Choose Valid one!" ;fi
				echo $i 	
		esac	

done